.. _edge_administration:

*******************
Edge Administration
*******************

.. contents:: :local:

Launch services
===============

.. code:: bash

    edge-dcomp up -d

See logs
========

By default, logs are configured to keep 200M of log per service (5 files
of 40M each).

You can see logs with the following command

.. code:: bash

    edge-dcomp logs -tf

This one to see the last 1000 lines of logs for the nginx service

.. code:: bash

    edge-dcomp logs -tf --tail=1000 nginx

Web Proxy Logs
--------------

Rate limiting
^^^^^^^^^^^^^

When a request is rate limited nginx answers with status **429**.

::

    nginx_1   | 2021-03-03T09:52:43.227539911Z 81.185.165.93 - - [03/Mar/2021:09:52:43 +0000] "GET / HTTP/1.1" 429 169 "-" "Mozilla/5.0 (pc-x86_64-linux-gnu) Siege/4.0.4" "-"

You will also see an error log with the zone that was limited, the
client address etc.:

::

    nginx_1   | 2021-03-03T09:52:39.122434232Z 2021/03/03 09:52:39 [error] 41#41: *31 limiting requests, excess: 20.440 by zone "webapp", client: 81.185.165.93, server: xivo-edge-access.dev.avencall.com, request: "GET / HTTP/1.1", host: "xivo-edge-access.dev.avencall.com"

TURN Server Logs
----------------

Examples of logs:

**TURN Allocation Request**

-  91.194.178.211 is the client
-  91.194.178.210 is the TURN server

::

    coturn_1  | 2021-03-03T12:24:25.079776718Z 361: : IPv4. tcp or tls connected to: 91.194.178.211:45575
    coturn_1  | 2021-03-03T12:24:25.079812744Z 361: : IPv4. tcp or tls connected to: 91.194.178.211:45575
    coturn_1  | 2021-03-03T12:24:25.079906231Z 361: : session 000000000000000005: realm <dev.avencall.com> user <>: incoming packet message processed, error 401: Unauthorized
    coturn_1  | 2021-03-03T12:24:25.079948400Z 361: : session 000000000000000005: realm <dev.avencall.com> user <>: incoming packet message processed, error 401: Unauthorized
    coturn_1  | 2021-03-03T12:24:25.080959417Z 361: : IPv4. Local relay addr: 91.194.178.210:39824
    coturn_1  | 2021-03-03T12:24:25.080979712Z 361: : IPv4. Local relay addr: 91.194.178.210:39824
    coturn_1  | 2021-03-03T12:24:25.080984747Z 361: : session 000000000000000005: new, realm=<dev.avencall.com>, username=<1614779903>, lifetime=600
    coturn_1  | 2021-03-03T12:24:25.080988937Z 361: : session 000000000000000005: new, realm=<dev.avencall.com>, username=<1614779903>, lifetime=600
    coturn_1  | 2021-03-03T12:24:25.081354852Z 361: : session 000000000000000005: realm <dev.avencall.com> user <1614779903>: incoming packet ALLOCATE processed, success
    coturn_1  | 2021-03-03T12:24:25.081375101Z 361: : session 000000000000000005: realm <dev.avencall.com> user <1614779903>: incoming packet ALLOCATE processed, success

Troubleshooting
===============

XiVO
----

ICE negotiation debug in asterisk
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

In asterisk CLI run command:

::

    core set debug 2 res_rtp_asterisk.so

Then look in asterisk full logs. You'll see logs like:

::

    [Mar 25 18:23:58] DEBUG[1806][C-0000001d] res_rtp_asterisk.c: (0x7ff5182e4a30) RTP allocated port 15094
    [Mar 25 18:23:58] DEBUG[1806][C-0000001d] res_rtp_asterisk.c: (0x7ff5182e4a30) ICE creating session 0.0.0.0:15094 (15094)
    [Mar 25 18:23:58] DEBUG[1806][C-0000001d] res_rtp_asterisk.c: (0x7ff5182e4a30) ICE create
    [Mar 25 18:23:58] DEBUG[1806][C-0000001d] res_rtp_asterisk.c: (0x7ff5182e4a30) ICE add system candidates
    [Mar 25 18:23:58] DEBUG[1806][C-0000001d] res_rtp_asterisk.c: (0x7ff5182e4a30) ICE add candidate: 10.32.4.2:15094, 2130706431
    [Mar 25 18:23:58] DEBUG[1806][C-0000001d] res_rtp_asterisk.c: (0x7ff5182e4a30) ICE request TURN TCP RTP candidate
    [Mar 25 18:23:58] DEBUG[1806][C-0000001d] res_rtp_asterisk.c: (0x7ff5182e4a30) ICE add candidate: 10.32.0.5:39715, 16777215

In the log above:

* 10.32.4.2 is the asterisk local IP address
* 10.32.0.5 is the TURN IP address

Error sending STUN request log
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

If you see this log:

::

    [Mar 25 16:21:14] ERROR[1797]: pjproject: <?>:     icess0x7ff518338828 ..Error sending STUN request: Network is unreachable

it means that XiVO can't reach (at the network level) one of the peer
candidates. But it would that the xivo doesn't have network route toward
this peer candidate. It should not happen if the XiVO is correctly
configured with a default route.

.. _edge_tblting_xivo_relaypermission:

Relay Permission Error
^^^^^^^^^^^^^^^^^^^^^^

The TURN server is by default configured to not relay towards internal network. One must whitelist the at least the XIVO IP (and MDS and Meetingroom server if you have one) - see TURN section of :ref:`edge_config_turnrelaypermission`.

If you see these logs **for an IP** of **either the XIVO or the MDS or the Meetingroom server** then it **may** mean that you didn't configure the relay permission correctly:

::

  [Oct 18 18:32:03] ERROR[24183] pjproject:         tcprel0x7f7c343f59a0 .CreatePermission failed for IP 10.32.0.1: 403/Forbidden IP

For example in the log above the coturn server does not accept to relay traffic towards **10.32.0.1** IP address.
It's ok as long as **10.32.0.1** is not one of the XIVO or the MDS (or the Meetingroom) server IP address.

See :ref:`edge_tblting_turn_relaypermission` for the corresponding logs in TURN Server.

TURN Server
-----------

.. _edge_tblting_turn_relaypermission:

Relay Permission Error
^^^^^^^^^^^^^^^^^^^^^^

The TURN server is by default configured to not relay towards internal network. One must whitelist the at least the XIVO IP (and MDS and Meetingroom server if you have one) - see TURN section of :ref:`edge_config_turnrelaypermission`.

If you see these logs **for an IP** of **either the XIVO or the MDS or the Meetingroom server** then it **may** mean that you didn't configure the relay permission correctly:

::

  coturn_1    | 2021-10-18T16:35:01.310299945Z 807: : ERROR: A peer IP 10.32.0.5 denied in the range: 10.0.0.0-10.255.255.255
  coturn_1    | 2021-10-18T16:35:01.326137547Z 807: : session 001000000000000017: realm <test.avencall.com> user <1634578239>: incoming packet CREATE_PERMISSION processed, error 403: Forbidden IP
  coturn_1    | 2021-10-18T16:35:01.326589074Z 807: : session 001000000000000017: realm <test.avencall.com> user <1634578239>: incoming packet message processed, error 403: Forbidden IP

For example in the log above the coturn server does not accept to relay traffic towards **10.32.0.5** IP address.
It's ok as long as **10.32.0.5** is not one of the XIVO or the MDS (or the Meetingroom) server IP address.

See :ref:`edge_tblting_xivo_relaypermission` for the corresponding logs in XiVO.

SIP Server
----------

To debug Kamailio you can raise log level of module `xlog` (for dbg log in kamailio script) and `tm` for relay operation::

  edge-dcomp exec kamailio bash
  kamcmd
  dbg.set_mod_level xlog 3
  dbg.set_mod_level tm 3
