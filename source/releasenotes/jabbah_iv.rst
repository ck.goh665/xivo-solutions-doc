*********************************
XiVO Jabbah Intermediate Versions
*********************************

2022.09 (IV Jabbah)
===================

Consult the `2022.09 (IV Jabbah) Roadmap <https://projects.xivo.solutions/versions/311>`_.

Components updated: **call-qualification**, **config-mgt**, **recording-server**, **xivo-agid**, **xivo-confgend**, **xivo-config**, **xivo-dao**, **xivo-db**, **xivo-monitoring**, **xivo-web-interface**, **xivocc-installer**, **xucmgt**, **xucserver**

**CCAgent**

* `#5695 <https://projects.xivo.solutions/issues/5695>`_ - Call qualification updates
* `#5744 <https://projects.xivo.solutions/issues/5744>`_ - Call qualification should be opened in full screen when displayed

**Chat**

* `#5387 <https://projects.xivo.solutions/issues/5387>`_ - Chat : when I receive a link in the chat, the link is not clickable + z-index refactor

**Desktop Assistant**

* `#5448 <https://projects.xivo.solutions/issues/5448>`_ - [C] - Calls pickup with `*8` extension fails with Cti.Dial

  .. important:: **Behavior change** `*8` is not anymore supported by asterisk core feature but xivo-feature. Pay attention that migration sets it back to `*8`

* `#5623 <https://projects.xivo.solutions/issues/5623>`_ - Switchboard in electron : window resize makes ongoing video too small

**Mobile Application**

* `#5753 <https://projects.xivo.solutions/issues/5753>`_ - Mobile application documentation

**Recording**

* `#5211 <https://projects.xivo.solutions/issues/5211>`_ - Translate Recording Server Interface in English
* `#5244 <https://projects.xivo.solutions/issues/5244>`_ - [C] - Recording - Cannot download access logs when it is too big

**Switchboard**

* `#5389 <https://projects.xivo.solutions/issues/5389>`_ - Calling using keyboard on switchboard and cc agent does not work - Jabbah

**Web Assistant**

* `#5108 <https://projects.xivo.solutions/issues/5108>`_ - Can't relogin when we get the new version message on login page
* `#5172 <https://projects.xivo.solutions/issues/5172>`_ - History and presence status display as well as avatar
* `#5660 <https://projects.xivo.solutions/issues/5660>`_ - When I search for a user without line number, I still see the dialing icon
* `#5700 <https://projects.xivo.solutions/issues/5700>`_ - User with just email and no Number still have the chevron displayed near Email Icon

**XiVO PBX**

* `#5037 <https://projects.xivo.solutions/issues/5037>`_ - [Webi - Device] Configuration model changes when we put a device in autoprov mode
* `#5055 <https://projects.xivo.solutions/issues/5055>`_ - Outcall route in Webi are not displayed in order of select
* `#5255 <https://projects.xivo.solutions/issues/5255>`_ - IVR Can't access list of IVR scenarios from users edition
* `#5614 <https://projects.xivo.solutions/issues/5614>`_ - UA - Make work UA account for call groups, group pickup, and boss secretary filter

  .. important:: **Behavior change** Cannot switch to "Local" or "default" channel anymore in addition or edition of users in the "groups" tab. DND will now always apply to groups if feature is enabled for the user.

* `#5639 <https://projects.xivo.solutions/issues/5639>`_ - IVR Incoming call
* `#5720 <https://projects.xivo.solutions/issues/5720>`_ - monit generate high disk latency and high cpu usage
* `#5752 <https://projects.xivo.solutions/issues/5752>`_ - XDS - inter mds trunk fails to authenticate (PJSIP)
* `#5756 <https://projects.xivo.solutions/issues/5756>`_ - Create migration script to update interfaces
* `#5759 <https://projects.xivo.solutions/issues/5759>`_ - Duplicate migration script prevent to upgrade Izar to Jabbah
* `#5763 <https://projects.xivo.solutions/issues/5763>`_ - Boss/Secretary filter does not work with PJSip

2022.08 (IV Jabbah)
===================

Consult the `2022.08 (IV Jabbah) Roadmap <https://projects.xivo.solutions/versions/309>`_.

Components updated: **asterisk**, **edge-nginx**, **sipml5-xivo-mirror**, **xivo-confd**, **xivo-confgend**, **xivo-config**, **xivo-db**, **xivo-dird**, **xivo-monitoring**, **xivo-proxy**, **xivo-service**, **xivo-sysconfd**, **xivo-web-interface**, **xivocc-installer**, **xucmgt**, **xucserver**

**Asterisk**

* `#5289 <https://projects.xivo.solutions/issues/5289>`_ - Handle registration from unknown peer with PJSIP
* `#5438 <https://projects.xivo.solutions/issues/5438>`_ - Build asterisk 18.10.1-1-xivo3 with patch #5196 and #5289
* `#5440 <https://projects.xivo.solutions/issues/5440>`_ - XDS - prevent loops between MDS (dialplan)

**CCAgent**

* `#5447 <https://projects.xivo.solutions/issues/5447>`_ - Jitsi on top of third party application

**Desktop Assistant**

* `#5583 <https://projects.xivo.solutions/issues/5583>`_ - Third party application is not displayed in desktop mode

**Mobile Application**

* `#5416 <https://projects.xivo.solutions/issues/5416>`_ - Mobile App - Dialplan does not loop correctly over contact list
* `#5497 <https://projects.xivo.solutions/issues/5497>`_ - Cannot complete attended transfer from mobile when both UC and mobile are used

**Security**

* `#4422 <https://projects.xivo.solutions/issues/4422>`_ - Meeting Room Audit - /video

**Web Assistant**

* `#5543 <https://projects.xivo.solutions/issues/5543>`_ - As an english user I want to see the mobile app association gif in english 

**WebRTC**

* `#5373 <https://projects.xivo.solutions/issues/5373>`_ - Cannot make webrtc audio calls with Chrome 103 - Jabbah

**XUC Server**

* `#5344 <https://projects.xivo.solutions/issues/5344>`_ - xucserver - function isSipAttTransfer works only with chan_sip
* `#5460 <https://projects.xivo.solutions/issues/5460>`_ - PJSIP - Missing SIPCALLID sometimes in PhoneEvents
* `#5496 <https://projects.xivo.solutions/issues/5496>`_ - PJSIP - Missing call tracking when agent makes call from his deskphone (Snom or Yealink)

**XiVO PBX**

* `#4865 <https://projects.xivo.solutions/issues/4865>`_ - Directory lookup: search result order given by server is not followed by frontend
* `#5165 <https://projects.xivo.solutions/issues/5165>`_ - [Doc] Add a note for call qualifications that it requires a 3rd party app for agent to use qualifiactions
* `#5266 <https://projects.xivo.solutions/issues/5266>`_ - Nginx bridged mode - make work phonebook search and search via phone [Jabbah]
* `#5281 <https://projects.xivo.solutions/issues/5281>`_ - XDS / PJSIP - Be able to call an outgoing call via another MDS
* `#5382 <https://projects.xivo.solutions/issues/5382>`_ - Add a proxy in docker net to cleanly finish webi in docker bridge
* `#5395 <https://projects.xivo.solutions/issues/5395>`_ - Running wizard with handle_system_conf false should not touch /etc/hosts
* `#5424 <https://projects.xivo.solutions/issues/5424>`_ - agent login (\*30) does not work from UC app (with Cti.Dial)
* `#5450 <https://projects.xivo.solutions/issues/5450>`_ - Favorites are not sorted properly [Jabbah]
* `#5461 <https://projects.xivo.solutions/issues/5461>`_ - Automatic modification of the login field with lower case through CSV update/import
* `#5471 <https://projects.xivo.solutions/issues/5471>`_ - [Doc] Document to prefer using xivo-pickup instead of basic Answer for custom dialplans (IVR) - and adapt IVR dialplan samples
* `#5473 <https://projects.xivo.solutions/issues/5473>`_ - Sorting of accented characters [Jabbah]

  .. important:: **Behavior change** Lookup results are now ordered in way to prefer more relevant results (see :ref:`dird_plugin_view_defaultjson`):
    
     #. exact matches of whole word
     #. exact matches at beginning of word
     #. exact matches anywhere
     #. matches similar to term

* `#5495 <https://projects.xivo.solutions/issues/5495>`_ - Missing xds max foward header on chansip calls
* `#5536 <https://projects.xivo.solutions/issues/5536>`_ - PJSIP - Verify and adapt some sip options for pjsip
* `#5542 <https://projects.xivo.solutions/issues/5542>`_ - [IVR] Include a xivo-pickup prior the call to Graphical IVR AGI

2022.07 (IV Jabbah)
===================

Consult the `2022.07 (IV Jabbah) Roadmap <https://projects.xivo.solutions/versions/303>`_.

Components updated: **edge-kamailio**, **xivo-acceptance**, **xivo-agid**, **xivo-confd**, **xivo-confgend**, **xivo-config**, **xivo-dao**, **xivo-db**, **xivo-lib-python**, **xivo-monitoring**, **xivo-sysconfd**, **xivo-upgrade**, **xivo-web-interface**, **xivocc-installer**, **xucmgt**, **xucserver**

**Desktop Assistant**

* `#4211 <https://projects.xivo.solutions/issues/4211>`_ - Desktop App - Handle right-clic menu (to paste or copy - for example in MR chat)

**Reporting**

* `#5173 <https://projects.xivo.solutions/issues/5173>`_ - SpagoBI - improve datatypes for XLS export in sample reports

**Switchboard**

* `#4461 <https://projects.xivo.solutions/issues/4461>`_ - [UC Switchboard] Make Switchboard/POPC possible in Desktop Applications

**Web Assistant**

* `#5283 <https://projects.xivo.solutions/issues/5283>`_ - Stop ringing on assistant when the call was answered on mobile

**WebRTC**

* `#5175 <https://projects.xivo.solutions/issues/5175>`_ - Information message you do not have an active voicemail when calling \*98

**XUC Server**

* `#5267 <https://projects.xivo.solutions/issues/5267>`_ - When dialing from UC, we should not make ring Mobile APP
* `#5269 <https://projects.xivo.solutions/issues/5269>`_ - [C] - Cannot transfer because of ghost calls in xuc when fallback to trunk on another mds
* `#5344 <https://projects.xivo.solutions/issues/5344>`_ - xucserver - function isSipAttTransfer works only with chan_sip

**XiVO PBX**

* `#5060 <https://projects.xivo.solutions/issues/5060>`_ - Change webi mode host to mode bridge
* `#5232 <https://projects.xivo.solutions/issues/5232>`_ - Dotenv issue in xivo-confgend logs
* `#5286 <https://projects.xivo.solutions/issues/5286>`_ - We should a message in user configuration page if you have no labels
* `#5294 <https://projects.xivo.solutions/issues/5294>`_ - Remove dotenv requirements from xivo-lib-python and other projects
* `#5353 <https://projects.xivo.solutions/issues/5353>`_ - Labels - when more than 150 labels, labels selection component in user form is broken
* `#5354 <https://projects.xivo.solutions/issues/5354>`_ - Mobile application: 2 registers on the Xivo side

**XiVOCC Infra**

* `#5284 <https://projects.xivo.solutions/issues/5284>`_ - Cannot make ring UC and mobile app at the same time

2022.06
=======

Consult the `2022.06 Roadmap <https://projects.xivo.solutions/versions/299>`_.

Components updated: **xivo-confd**, **xivo-config**, **xivo-dao**, **xivo-db**, **xivo-monitoring**, **xivo-web-interface**, **xivo-webi-nginx**, **xivocc-installer**

**XiVO PBX**

* `#4723 <https://projects.xivo.solutions/issues/4723>`_ - Change nginx mode host to mode bridge
* `#5198 <https://projects.xivo.solutions/issues/5198>`_ - Add option to wizard API to bypass network configuration
* `#5233 <https://projects.xivo.solutions/issues/5233>`_ - Add an option in wizard API to be able to run post-wizard scripts
* `#5242 <https://projects.xivo.solutions/issues/5242>`_ - Improve web-interface labels with filtering and better display for long lists
* `#5246 <https://projects.xivo.solutions/issues/5246>`_ - IVR access is broken
* `#5258 <https://projects.xivo.solutions/issues/5258>`_ - Labels page does not work with latest nginx
