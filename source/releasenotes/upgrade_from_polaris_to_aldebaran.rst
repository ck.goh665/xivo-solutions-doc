****************************
Upgrade Polaris to Aldebaran
****************************

In this section are listed the manual steps to do when migrating from Polaris to Aldebaran.

.. contents::

Before Upgrade
==============

* If docker is installed (on XiVO PBX or XiVO CC), its custom start options may cause the upgrade to fail. Please check that ``/etc/systemd/system/docker.service.d``
  directory does not exist or it is empty. Otherwise see documentation on `docker web <https://docs.docker.com/config/daemon/systemd/#custom-docker-daemon-options>`_.

On XiVO PBX
-----------

* XiVO Aldebaran is only available in **64-bit** versions. To migrate XiVO from ``i386`` to ``amd64``, you must follow :ref:`migrate_i386_to_amd64`.
* Configuration files and dialplan subroutine for ACD outgoing calls for call blending
  (See `Polaris documentation <https://documentation.xivo.solutions/en/2017.11/administrator/xivocc/system_configuration.html#acd-outgoing-calls-for-call-blending>`_)
  was integrated to XiVO.
  The configuration **must** be manually removed if it exists::

    sed -i '/generate_agent_skills.py/d' /etc/asterisk/queueskills.conf
    rm /usr/local/sbin/generate_agent_skills.py
    rm /etc/asterisk/extensions_extra.d/xuc_outcall_acd.conf


* If the XiVO was added to XiVO Centralized User Management, the file ``routage.conf`` **must** be updated.

  * Download the new `routage.conf <https://gitlab.com/xivo-utils/icdu-packaging/raw/a53e1fe2411ff5ed85dc54e61d135dc578c2a91f/interface-centralisee/xivo_config/routage.conf/>`_ file
  * Replace the old file ``/etc/asterisk/extensions_extra.d/routage.conf`` by the new one

* Follow the :ref:`upgrade` page.

On XiVO CC
----------

Nothing specific, follow the :ref:`upgrade_cc` page.


After Upgrade
=============

On XiVO PBX
-----------

* Outgoing call was removed from redirections.

  You MUST check the xivo-upgrade log with the following command::

    zgrep MIGRATE_OUTCALL_FWD /var/log/xivo-upgrade.log*

  Then you MUST edit each object to reconfigure the changed destination.
  All Outgoing call redirections can be replaced by an Extension redirection with the appropriate context.
  For example an outgoing redirection towards "my_outcall (@to-extern)" to number "0123456789"
  can be replaced by an extension redirection to "0123456789" with context "to-extern"

* Callbacks: callback API URL was changed. It is now prefixed with `configmgt`. If you have any AGI calling the callbacks API
  (for example `api/1.0/callback_lists`) you MUST add the prefix `configmgt` to it : `configmgt/api/1.0/callback_lists`.

* ConfigMgt service was moved to XiVO PBX. Here's how to migrate the ConfigMgt database:

  * On your **XiVO CC** (where the service pgxivocc is run) authorize user root to login with password via ssh,
  * Then on your **XiVO CC**, stop the services with ``xivocc-dcomp stop``
  * Then, on your **XiVO PBX**, run the migration script and follow the instruction::

     xivo-migrate-configmgt-db

  * Finally, when migration is complete, relaunch the services on **XiVO CC**::

     xivocc-dcomp up -d --remove-orphans


On XiVO CC
----------

* Update new kibana default panels::

    xivocc-dcomp stop kibana_volumes
    docker rm -v xivocc_kibana_volumes_1
    xivocc-dcomp up -d



