.. _ivr-editor:

*************
IVR Editor
*************

IVR is a new feature introduced in the Helios LTS.

.. important:: This is an enterprise version feature that is not included in XiVO and is not freely available.
    To enable it, please contact `XiVO team <https://www.xivo.solutions/contact/>`_.

It allows to create part of dialplan in a graphical editor.

**Installation is described below:**

.. toctree::
   :maxdepth: 2

   install

**For feature description and users guide see below:**

.. contents:: :local:

.. _ivr_features:

Features
========

* Create a edit part of dialplan (IVR) in graphical editor.
* Create caller hangup handler and error handler IVR dialplan branch
* Use created IVRs as an incall or a schedule destination
* Send call to queue or other defined dialplan object after IVR
* Upload and manage IVR specific audio files (voice prompts)

.. _ivr_usersguide:

Users' Guide
============

IVR management
--------------

In menu Services->IPBX in Call management section use
option IVR. The page shows list of existing IVRs. Buttons in every IVR line
allows you to edit IVR dialplan metadata, edit dialplan and delete dialplan.

Graphical dialplan editor
-------------------------

Dialplan flowchart consists of nodes representing dialplan actions and
connections between nodes. Every node type has one entry point (large circle on top of the node
rectangle) and this entry point accepts many connection lines. Exceptions are nodes Start, OnHangup and OnError
which have no entrypoint. Depending on node type, nodes have zero to many exit points (small circles
at  bottom of node rectangle). Exit point of node is selected at runtime by
result of node action.

In editor you can

* Add node to editor pane by clicking on node type in left menu.
* Delete node by clicking red cross on node
* Set node parameters by clicking pencil on top left node corner anf filling form
* Interconnect nodes by dragging exit point of one node to entry point of another or same node
* Delete connection line by clicking on rectangle with minus sign on the line

IVR dialplan runtime
--------------------

IVR dialplan always starts on Start node. This node can be only one in
flowchart and cannot be deleted. If caller hangs up in IVR dialplan, call is
moved to OnHangup node, if one exists in flowchart. Only one OnHangup node
can exists in flowchart. If runtime error happens (nonexistent voice prompt
file, wrong expression parameter) call is sent to the OnError node if one
exists. Only one OnError node can exist in flowchart.


Variables
---------

Variables in IVR dialplan are NOT the same thing as asterisk dialplan
variables. Variables can be set at runtime using assign node or as result
of node action. Variable name must start with '$' (dollar sign).

Expressions in node parameters
------------------------------

Node parameters can be constants or expressions.
For example node type 'wait' has parameter 'seconds' which defines time to
wait. This parameter can contain

``2			constant``

``= 2 + 2		expression``

``= 10 + $var		expression with variable``

String expression can contain string constants in apostrophes or quotes,
variables and concatenation operator '.'.
Numeric expressions can contain integer numbers, variables and operators '+', '-', '*', '/', 
Logical expressions can contain comparision operators '==', '!=', '<',
'<=', '>', '<', '>=' and logical operators '!', '&&', '||'.

Expressions in node parameters are evaluated at runtime when node action
starts.

Some input fields are listboxes by nature, e.g. voice prompt file is selected
from existing files. In such case listbox can be swithced to text input field by
clicking to icon next the field label, to allow insert expresion.

E.g. if you have voice prompt files 

* message-en
* message-fr
* message-de

you can set variable $lang in dialplan and then expression::

= "message-" . $lang

allows you to use right language variant of message.


Node types
----------

start
^^^^^

Starting node of IVR dialplan. It is created automatically and cannot be
deleted. Only one node of type start can exist in flowchart.

**Parameters**
*Node has no parameters*

**Exit points**
* next

onhangup
^^^^^^^^

This node starts flowchart part which is used after caller hangup,
The node has no entry point therefore.
Only one node of type onhangup can be present in flowchart.

**Parameters**

*Node has no parameters*

**Exit points**

* next



onerror
^^^^^^^

This node starts flowchart part which is used after runtime error.
The node has no entry point therefore.
Only one node of type onerror can be present in flowchart.

**Parameters**

*Node has no parameters*

**Exit points**

* next


assign
^^^^^^

Assigns value to variable

**Parameters**

* Variable: variable name
* Value: value to assign

**Exit points**

* next


cel
^^^

Runs CELGenUserEvent dialplan application

**Parameters**

* Name: event name
* Message: Extra text to be included with the event

**Exit points**

* next

condition
^^^^^^^^^

Evaluates condition and continues in flowchart according the result

**Parameters**

* Condition: condition to evaluate

**Exit points**

* 1: used when condition is evaluated as true
* 0: used when condition is evaluated as false

hangup
^^^^^^

Hangups the call and exits flowchart AGI script

**Parameters**

*Node has no parameters*

**Exit points**

*Node has no exits*


http
^^^^

Sends HTTP(S) request
For GET request variables are used in URL query string
For POST request variables are sent as application/x-www-form-urlencoded data
Variable names in request are used without starting dollar sign.

**Parameters**

* URL: URL of the request
* Method: Request method
* Variables: space separated list of variables used in request 

**Exit points**

* next

menu
^^^^

#. Plays file set in parameter 'Menu voice prompt'
#. Waits for DTMF digit, waiting is limited by 'Timeout' parameter
#. If DTMF digit is received and is listed in 'Valid options' parameter, corresponding node exit point is used.
#. If DTMF digit received is not listed in 'Valid options' parameter, file defined in parameter 'Invalid voiceprompt' is played and node is started again, unless number of tries defined in parameter 'Repeat' is reached.
#. If no DTMF digit is received till timeout, file defined in parameter 'No choice voiceprompt' is played and and node is started again, unless number of tries defined in parameter 'Repeat' is reached.
#. Node uses 'f' exit 

**Parameters**

* Repeat: Number of attempts to receive DTMF digit
* Timeout: Time in seconds for which node waits for a DTMF digit
* Valid options: List of expected DTMF digits (without any delimiter)
* Menu voiceprompt: File played at node start
* No choice voiceprompt: File played when no digit has been sent
* Invalid voiceprompt: File played when unexpected DTMF digit has been sent

**Exit points**

* 0-9,A-D,*,#: used when corresponding DTMF digit has been sent and accepted by node
* f: used when no valid choice has been sent


playback
^^^^^^^^

Play selected file

**Parameters**

* Voice prompt file: file to play

**Exit points**

* next


queue 
^^^^^

Exit points flowchart AGI script and send dialplan to selected queue

**Parameters**

* Name: queue name

**Exit points**

*node has no exit point, controll is passed to asterisk dialplan*

read 
^^^^

Reads sequens of DTMF digits terminated by '#'

**Parameters**

* Voice prompt file: File played at node start
* Invalid voice prompt: File played when invalid input has been sent
* Repeat: Max. number of attempts
* Min digits: Minimal number of digits in input
* Max digits: Maximal number of digits in input
* Numeric: If checked numeric value is expected
* Min value: Minimal value of numeric input
* Max value: Maximal value of numeric input
* Beep: Beep before input
* Variable: Store input in this variable

**Exit points**

* next

saynumber
^^^^^^^^^

Says number using Saynumber asterisk dialplan application

**Parameters**

* Number: Number to be said

**Exit points**

* next

wait
^^^^

Wait for defined number of seconds. Wait asterisk dialplan application is used.

**Parameters**

* Seconds: Number of seconds to wait

**Exit points**

* next
