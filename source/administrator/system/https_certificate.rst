.. _https_certificate:

*****************
HTTPS certificate
*****************

.. contents:: :local:


X.509 certificates are used to authorize and secure communications with the server. They are mainly
used for HTTPS, but can also be used for SIPS, CTIS, WSS, etc.

There are two certificates shipped by default in XiVO:

* :file:`/usr/share/xivo-certs/server.crt`: the default certificate used for backend services and REST APIs
* :file:`/etc/docker/nginx/ssl/xivoxc.crt`: the default certificates used by the web interface
  (and also by the UC Application when in :ref:`UC Addon mode <xivouc_addon>`)


Default certificate
===================

XiVO uses HTTPS where possible. The certificates are generated at install time. The main certificates are located

* in :file:`/usr/share/xivo-certs/server.crt` (used by backend services and REST APIs)
* and in :file:`/etc/docker/nginx/ssl/xivoxc.crt` (used ny Nginx and therefore the Webi)

However, these certificates are self-signed, and HTTP clients (browser or REST API client) will
complain about this default certificate because it is not signed by a trusted Certification
Authority (CA).


The default certificate is untrusted
------------------------------------

To make the HTTP client accept this certificate, you have two choices:

* configure your HTTP client to trust the self-signed XiVO certificate by adding a new trusted CA.
  The CA certificate (or bundle) is the file :file:`/usr/share/xivo-certs/server.crt`.
* replace the self-signed certificate with your own trusted certificate.


Regenerating Default Certificate
================================

.. _regenerating_nginx_certificate:

Default NGINX Certificate
-------------------------

.. warning:: If you use your own certificate, you should NOT replace it by the default certificate.

1. Regenerate the default certificate by this command::

    ssldir=/etc/docker/nginx/ssl
    openssl req -nodes -newkey rsa:2048 \
      -keyout "${ssldir}/xivoxc.key" \
      -out "${ssldir}/xivoxc.csr" \
      -subj "/C=FR/ST=Rhone-Alpes/L=Limonest/O=Avencall/CN=$(hostname --fqdn)"
    openssl x509 -req -days 3650 \
      -in "${ssldir}/xivoxc.csr" \
      -signkey "${ssldir}/xivoxc.key" \
      -out "${ssldir}/xivoxc.crt"

3. Reload Nginx service ``xivo-dcomp reload nginx``.

.. _regenerating_certificate:

Default Backend Certificate
----------------------------

.. warning:: If you use your own certificate, you should NOT replace it by the default certificate.

1. Regenerate the default certificate by this command::

     openssl req -x509 -sha256 -nodes -days 3650 -newkey rsa:2048 \
       -config "/usr/share/xivo-config/x509/openssl-x509.conf" \
       -keyout "/usr/share/xivo-certs/server.key" \
       -out "/usr/share/xivo-certs/server.crt"

2. Change ownership and permissions::

     chown root:www-data "/usr/share/xivo-certs/server.key" "/usr/share/xivo-certs/server.crt"
     chmod 640 "/usr/share/xivo-certs/server.key" "/usr/share/xivo-certs/server.crt"

3. Restart all XiVO services by running ``xivo-service restart all``.

Replacing Default Certificate with a Trusted Certificate
========================================================

You can use a valid certificate for both Nginx service and Backend services.
Though, in production environement, **the main need** is to deploy a valid certificate for Nginx (and the UC-Addon app if you are in :ref:`UC Addon mode <xivouc_addon>`).
To do this follow :ref:`cert_change_xivonginx_cert`

If you want also to change the certificate for Backend service follow :ref:`cert_change_xivonbackend_cert`.

.. _cert_change_xivonginx_cert:

Install Trusted Certificate for Nginx (and UC app in UC Addon mode)
-------------------------------------------------------------------

This certificate is used for XiVO Webi and, *more importantly*, for UC Application when in :ref:`UC-Addon mode <xivouc_addon>`.
In this case this is mandatory (at least to use WebRTC) to install trusted certificate.

For this, follow these steps:

#. Replace the following files with your own private key/certificate pair:

   * Private key: :file:`/etc/docker/nginx/ssl/xivoxc.key`
   * Certificate: :file:`/etc/docker/nginx/ssl/xivoxc.crt`

#. Reload Nginx configuration with::

     xivo-dcomp reload nginx

#. Check that the certification chain is complete - see :ref:`nginx-complete-cert-chain`

.. _cert_change_xivonbackend_cert:

Install Trusted Certificate for Backend services
------------------------------------------------

.. note:: This is probably useless in most production environment.
   If you want to change the certificate on XiVO you probably want to change the NGINX one - see :ref:`cert_change_xivonginx_cert`.

If you want to use your certificate on backend services, follow these steps:

#. Replace the following files with your own private key/certificate pair:

   * Private key: :file:`/usr/share/xivo-certs/server.key`
   * Certificate: :file:`/usr/share/xivo-certs/server.crt`

#. Set correct ownership and permissions::

     chown root:www-data "/usr/share/xivo-certs/server.key" "/usr/share/xivo-certs/server.crt"
     chmod 640 "/usr/share/xivo-certs/server.key" "/usr/share/xivo-certs/server.crt"

#. Change the hostname of XiVO for each XiVO component: the different processes of XiVO heavily use
   HTTPS for internal communication, and for these connection to establish successfully, all
   hostnames used must match the Common Name (CN) of your certificate. Basically, you must replace
   all occurrences of ``localhost`` (the default hostname) with your CN in the :ref:`configuration of the
   XiVO services <configuration-files>`. For example::

      mkdir /etc/xivo/custom
      cat > /etc/xivo/custom/custom-certificate.yml << EOF
      consul:
        host: xivo.example.com
      auth:
        host: xivo.example.com
      confd:
        host: xivo.example.com
      dird:
        host: xivo.example.com
      ajam:
        host: xivo.example.com
      agentd:
        host: xivo.example.com
      EOF
      for config_dir in /etc/xivo-*/conf.d/ ; do
          ln -s "/etc/xivo/custom/custom-certificate.yml" "$config_dir/010-custom-certificate.yml"
      done

#. Also, you must replace ``localhost``, in the definition of your directories in the web interface under :menuselection:`Configuration --> Directories`, by the hostname matching the CN of your certificate.

#. Then, when done, you must re-save, the CTI Directories definition:

   * Go to :menuselection:`Services --> CTI Server --> Directories --> Definitions`
   * Edit each directory to re-select the new URI
   * And save it

#. If your certificate is not self-signed, and you obtained it from a third-party CA that is trusted
   by your system, you must enable the system-based certificate verification. By default,
   certificate verification is set to consider ``/usr/share/xivo-certs/server.crt`` as the only CA
   certificate.

   First you need to install the debian ``ca-certificates`` package::

     apt-get install ca-certificates

   If one of the CA (or intermediate CA) of your certificate is not present in the CA shipped by the
   ``ca-certificates`` package you will need to add it manually:

     * Create the following dir if not present::

        mkdir /usr/local/share/ca-certificates/
     * Copy inside this directory the certificate of the missing CA in a ``.crt`` file
     * And finally upload ca-certificates configuration::

        update-ca-certificates

   Then to activate the certificat verification, the options are the following:

   * Consul: ``verify: True``
   * Other XiVO services: ``verify_certificate: True``

   The procedure is the same as 2. with more configuration for each service. For example::

      cat > /etc/xivo/custom/custom-certificate.yml << EOF
      consul:
        host: xivo.example.com
        verify: True
      auth:
        host: xivo.example.com
        verify_certificate: True
      dird:
        host: xivo.example.com
        verify_certificate: True
      ...

   Setting ``verify_certificate`` to ``False`` will disable the certificate verification, but the
   connection will still be encrypted. This is pretty safe as long as XiVO services stay on the same
   machine, however, this is dangerous when XiVO services are separated by an untrusted network,
   such as the Internet.

#. Ensure your CN resolves to a valid IP address with:

   * a DNS entry
   * and an entry in :file:`/etc/hosts` resolving your CN to 127.0.0.1. Note that :file:`/etc/hosts`
     will be rewritten by xivo-sysconfd. To make the change persistent, you need to create **custom templates**:

     #. in :file:`/etc/xivo/custom-templates/system/etc/hosts` (based on template :file:`/usr/share/xivo-config/templates/system/etc/hosts`)
     #. and in :file:`/etc/xivo/sysconfd/custom-templates/resolvconf/hosts` (based on template :file:`/usr/share/xivo-sysconfd/templates/resolvconf/hosts`)

#. Your X.509 certificate must have ``subjectAltName`` defined. See the example at `cacert.org <http://wiki.cacert.org/FAQ/subjectAltName>`_
   or detailed documentation at `ietf.org <https://tools.ietf.org/html/rfc3280>`_.

#. Restart all XiVO services::

      xivo-service restart all
