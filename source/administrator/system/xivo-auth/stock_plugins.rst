.. _auth-stock-plugins:

===========================
Stock Plugins Documentation
===========================

.. _auth-backends:

Backends Plugins
================

XiVO Admin
-----------

Backend name: ``xivo_admin``

Purpose: Authenticate a XiVO administrator. The login/password is configured in
:menuselection:`Configuration --> Management --> Users`.


.. _auth-backends-service:

XiVO Service
------------

Backend name: ``xivo_service``

Purpose: Authenticate a XiVO :ref:`Web Services Access <web_services_access>`. The login/password is
configured in :menuselection:`Configuration --> Management --> Web Service Access`.


XiVO User
---------

Backend name: ``xivo_user``

Purpose: Authenticate a XiVO user. The login/password is configured in :menuselection:`IPBX -->
Services --> PBX Settings --> Users` in the CTI client section.


.. _auth-backends-session:

XiVO Session
------------

Backend name: ``xivo_session``

Purpose: Authenticate a XiVO administrator's session. This backend receives an administrator's webi cookie with session id and returns a valid token with a confd ACL
depending on the logged administrator webi permission:

  * Webi administrator of type *root* receives a wildcard confd ACL
  * Webi administrator of type *admin* receives a confd ACL depending on its webi permission (see table below)

Current mapping implemented:

+--------------------------------------------------+-----------------------+
| Webi Permission                                  | confd ACL             |
+==================================================+=======================+
| Services -> IPBX -> IPBX Settings -> User labels | ``confd.labels.#``    |
+--------------------------------------------------+-----------------------+
| Services -> IPBX -> IPBX Settings -> Users       | ``confd.labels.read`` |
+--------------------------------------------------+-----------------------+

See :ref:`auth-backends-session-webi-confd-mapping`.

.. _auth-backends-ldap:
