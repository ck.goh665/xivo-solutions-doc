***********************************
Xuc & Xucmgt (CC & UC applications)
***********************************

XUC overview page
=================

XUC overview page available at @XUC_IP:PORT, usually @SERVER_IP:8090. You have to check if the "Internal configuration cache database"
contains agents, queues etc.

XUC sample page
===============

XUC sample page available at @XUC_IP:PORT/sample, usually @SERVER_IP:8090/sample. You can use this page to check user login and other
API functions. CCManager, agent and assistant web use functions available on the sample page.

XUC Internal Metrics
====================

Internal metrics are also available - see :ref:`xuc_jmx_metrics` page.

.. toctree::
   :maxdepth: 2
   :hidden:

   jmx
   
Agent states after XUC restart
==============================

Please see the note in :ref:`restarting <agent_states_after_xuc_restart>` XUC server with active calls.

Queue created
=============

New queue will not appear immediately in :ref:`CCAgent <agent>` and :ref:`CCManager <ccmanager>` until
the agent or the manager is not relogged to these applications accordingly. The Xuc server restart **is not required**.

Queue deleted
=============

Deleted queue will still appear in :ref:`CCAgent <agent>` and :ref:`CCManager <ccmanager>` until the Xuc server **is restarted**.

.. _webrtc_quality_logging:

Audio quality issues
====================

If something goes wrong with the quality of an ongoing call, it will be reflected in live, every 20 seconds, inside the xuc server logs, with the following format::

   WARN  s.ClientLogger- [userw] 1590da900aba2a761573c0493c04a0a1@192.168.56.2:5060 Audio quality issues ->  RTT 186ms - Jitter upstream 1ms / downstream 0ms - Packet loss upstream 21% / downstream 6%

At the end of each call, a call quality report will also be sent in the xuc server logs, with the following format::

   INFO  s.ClientLogger- [userw] 1590da900aba2a761573c0493c04a0a1@192.168.56.2:5060 Call quality report -> Highest RTT 186ms - Highest Jitter upstream 3ms / downstream 3ms - Highest Packet loss upstream 21% / downstream 12%

Both those format include the user as well as the sip call id, so it can be grepped.
It can also be grepped by the type of message, "Audio quality issues" and "Call quality report".