.. _mobile_application:

******************
Mobile Application
******************

**Contents:**

.. contents:: :local:

.. _mobile_app_requirements:

Requirements
============

Minimum Izar version : Izar.09
------------------------------

The XiVO mobile application should be used with at least the IZAR version of XiVO. 
The XiVO must contain an EGDE infrastructure allowing teleworkers or travelling workers to be able to access XiVO from outside.
Follow the official documentation on required ports : See https://documentation.xivo.solutions/en/2022.10/edge/architecture.html#id3

.. figure:: images/mobile-app-schemas.png


When the user receives an incoming call, we use Firebase to wake up the mobile application by sending a push notification.
The official firebase documentation describes the required ports and firewall configuration.
See https://firebase.google.com/docs/cloud-messaging/concept-options#messaging-ports-and-your-firewall

Limitations
-----------

* No conferences
* No agent account 
* No unique account 

Compatibility
-------------

* Android: 

The tests are performed on Android 9, 10 and 11. Previous versions are not supported.
Officially supported devices :
  
  * Samsung :

    * Galaxy Note 9 (Android 10)
    * Galaxy A8 (Android 9) e
    * Galaxy A40 (Android 11)

  * Fairphone : FP3 (Android 11) 
  * Xiaomi : Note 9 

* IOS: 

The tests are performed on the version 15.5. 
The supported version is IOS version 15.
Previous versions are not supported.
On the following mobiles :

  * Iphone 11
  * Iphone XR
  * Iphone 13

Case of unlisted terminals 
---------------------------

Manufacturer’s software layer may create unattended behavior on unsupported devices. Please open a ticket to describe device - with OS version and the bug.

It is imperative t make preliminary tests on the mobiles which would not be compatible.

If you encounter problems, you can send an email to the support directly via the mobile application. It is however important to have configured on your mobile an application to send emails.
The logs of the mobile application are sent in an attached file in this email.

.. _mobile_app_push_notifications:

Push Notifications
==================

Push notification is a technology used in the XiVO server and the mobile application.

Since the Izar version, you can ring multiple devices at the same time. 
When you choose to ring the mobile application and the UCAssistant, the system will know when the devices are both ready to receive the incoming call.

As the UC assistant is connected directly to the server via the web interface, this will be relatively quick. 

For the mobile application, the operating systems (Android and IOS) are using a "DOZE" mode. 
This mode closes the application when it has not been used for some time, to reduce the consumption of the battery.
To wake up the application, we use the push notification that goes through a public push server. 
It will send a notification to the mobile to wake up the XiVO mobile application. 

You might need to wait a certain time after you launched the call for the mobile application to wake up. This time can vary from one to ten seconds, depending on the push servers and the network quality. 
Once the mobile application responds, we start ringing the available devices.

Some use cases
--------------

Ringing device selected: 

* *UC Assistant*: Fast ringing on the UC direct connection to the web browser or desktop
* *Mobile application*: Launching the ringtone on the mobile application when it will be awakened. The mode of operation used is the push notification. There can be a waiting time.
* *UC + Mobile Application*: There is a delay before launching the ringtone on both devices, the time for the mobile application to wake up.

.. figure:: images/push-notification.png

.. _mobile_app_troubleshooting:

Troubleshooting 
===============

Error Codes 
-----------

Error codes will be displayed in case of a problem on the application while trying to make an outgoing call : 

* Error code 1: 

This error code means the application is not receiving or sending the SIP signal, and that the user is not REGISTER in the asterisk.  

.. figure:: images/code-error1.png
   :scale: 80%

* Error code 2: 

You will get this error code if the application is able to receive or send the SIP signal, but the user is not REGISTER in the asterisk.

.. figure:: images/code-error2.png
   :scale: 80%

No internet access 
------------------

If the user looses access to the Internet, the application switches to a non-usable mode until access is regained (see screenshot below). 
This display is removed when there is an internet access.

.. figure:: images/no-internet-mobile-app.png
   :scale: 80%


Sending logs to support
-----------------------
If you encounter an unsual behaviour and you manage to reproduce it on your application, you can send the logs directly via the configuration and click on *Send logs to support*.

Warning: The logs are sent by email, you must therefore have a messaging system configured on your phone.

.. figure:: images/sending-logs-mobile-app.png
 :scale: 80%

.. figure:: images/sending-logs-mobile-app2.png
 :scale: 80%

Required permissions 
--------------------

If you didn't give all the authorizations needed to use the mobile app, this error message will be displayed the next time you open the application.
This means the XiVO phone account is not active.

.. figure:: images/required-permissions-mobile-app.png
 :scale: 80%

In this case, you should click the *OK* button and will be redirected to the settings.

If the phone is a "Samsung", the *phone accounts* settings will be opened. You just have to activate the check mark next to XiVO.

.. figure:: images/authorizations-mobile-app.png
 :scale: 80%

If the phone is *not* a Samsung, you will be redirected to the wrong settings. You will need to search in your phone settings where to activate the XiVO phone account.

Xiaomi stop battery optimization 
--------------------------------

.. figure:: images/xiaomi-battery-stop.png
 :scale: 80%

Xiaomi authorizations 
---------------------

In the settings of the Xivo application check if these settings are active: 

Show on lock screen
Show pop-up windows while browsing in the background

.. figure:: images/xiaomi-authorizations.png

If you are in the above context, you may not receive calls when your phone is locked.

Xiaomi phone account
--------------------

When activating the XiVO phone account (step 5), you need to : 

* Click *Ok* on the window  *Les permissions requises*, 
* go to *paramètres d’appel* => *Comptes des appels*,
* then in *comptes téléphoniques*  you need to activate XiVO. 

.. figure:: images/xiaomi-phone-accounts.png
 :scale: 80%

.. figure:: images/xiaomi-phone-accounts2.png
 :scale: 80%
