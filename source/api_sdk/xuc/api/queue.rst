.. _queue_api:

*********
Queue API
*********

This API is dedicated to manipulate queues for CC activities.

A sample of implementation is available in `app/assets/javascripts/pages/sampleQueues.js`
and `app/views/sample/sampleQueues.scala.html`

Queue Methods
=============
Cti.subscribeToQueueStats()
---------------------------

This command subscribes to the queue statistics notifications. First, all actual statistics values are sent for
initialisation and then a notification is sent on each update. Both initial values and updates are transmitted
by the QUEUESTATISTICS events.

Cti.subscribeToQueueCalls(queueId)
----------------------------------

Cti.unSubscribeToQueueCalls(queueId)
------------------------------------

Cti.retrieveQueueCall(queueCall)
--------------------------------

Allow to pickup one call from queue and connect it to current agent connected.

a QueueCall is one element of `Queue Calls`_, i.e

::

    {"position":1,"name":"John Doe","number":"33356782212","queueTime":"2015-07-16T10:40:16.626+02:00"}


.. warning:: Retrieve a queue call may fail if your device is already ringing or connected.

Queue Events
============
Queue Statistics
----------------
* Handler on :  Cti.MessageType.QUEUESTATISTICS

The handler is executed when a notification of new statistic values is received. Each message contains one or more
counters for one queue. The queue is identified by its queueId. See example below for reference.
The queue's id can be used to retrieve queue's configuration, see `Queue Configuration`_.

Following counters are available:

* TotalNumberCallsEntered
* TotalNumberCallsAnswered
* PercentageAnsweredBefore15
* TotalNumberCallsAbandonned
* TotalNumberCallsAbandonnedAfter15
* PercentageAbandonnedAfter15
* WaitingCalls
* LongestWaitingTime
* EWT
* AvailableAgents
* TalkingAgents

::

    {
        "msgType":"QueueStatistics",
        "ctiMessage":{
            "queueId":11,"counters":[{"statName":"AvailableAgents","value":0},{"statName":"LoggedAgents","value":0},{"statName":"TalkingAgents","value":0},{"statName":"EWT","value":0}]
        }
    }

Some events contain a queueRef with a queue's name instead of the queueId. This issue should be eliminated in
future versions.

::

    {"queueRef":"travels","counters":[{"statName":"TotalNumberCallsAbandonned","value":19}]}

Queue Calls
-----------
* Handler on: Cti.MessageType.QUEUECALLS

Awaiting calls in a queue. Subscription to the events with : `Cti.subscribeToQueueCalls(9)` (9 being the queueId).
Unsubscription with: `Cti.unSubscribeToQueueCalls(9)`.

::

    {"queueId":9,"calls":[{"position":1,"name":"John Doe","number":"33356782212","queueTime":"2015-07-16T10:40:16.626+02:00"}]}

.. _queue_configuration_api:

Queue Configuration
-------------------
* QUEUECONFIG : "QueueConfig",

::

    {"id":8,"context":"default","name":"blue","displayName":"blue sky","number":"3506","recording_mode":"recorded","recording_activated":1}

Queue List
----------
* QUEUELIST : "QueueList",

::

    {
        "msgType":"QueueList",
        "ctiMessage":[
            {"id":170,"context":"default","name":"bluesky","displayName":"Bl Record","number":"3012"},
            {"id":5,"context":"default","name":"noagent","displayName":"noagent","number":"3050"},
            {"id":6,"context":"default","name":"__switchboard_hold","displayName":"Switchboard hold","number":"3005"},
            {"id":173,"context":"default","name":"outbound","displayName":"outbound","number":"3099"},
            {"id":2,"context":"default","name":"yellow","displayName":"yellow stone","number":"3001"},
            {"id":7,"context":"default","name":"green","displayName":"green openerp","number":"3006"},
            {"id":3,"context":"default","name":"red","displayName":"red auto polycom","number":"3002"},
            {"id":11,"context":"default","name":"pool","displayName":"Ugips Pool","number":"3100"},
            {"id":4,"context":"default","name":"__switchboard","displayName":"Switchboard","number":"3004"}
            ]
    }

.. _queue_member_api:

Queue Member
------------
* Handler on : Cti.MessageType.QUEUEMEMBER

Received when an agent is associated to a queue or a penalty is updated. Penalty is -1 when agent is removed from a queue

::

    {"agentId":19,"queueId":3,"penalty":12}

Queue Member List
-----------------
* Handler on : Cti.MessageType.QUEUEMEMBERLIST

::

    {
        "msgType":"QueueMemberList",
        "ctiMessage":[
            {"agentId":129,"queueId":8,"penalty":2},
            {"agentId":139,"queueId":168,"penalty":2},
            {"agentId":129,"queueId":10,"penalty":0},
            {"agentId":129,"queueId":11,"penalty":0}
            ]
    }

Membership Methods
==================

Membership.init(cti)
--------------------

Initialize the Membership library using the given Cti object.

Membership.getUserDefaultMembership(userId)
-------------------------------------------

Request the default membership for the given user id. Warning, the userId is not the same as the agentId.

Membership.setUserDefaultMembership(userId, membership)
-------------------------------------------------------

Set the default membership for the given user id. Warning, the userId is not the same as the agentId.
'membership' should be an array of Queue membership like:

::

   [
     {"queueId":8,"penalty":1},
     {"queueId":17,"penalty":0},
     {"queueId":18,"penalty":0},
     {"queueId":23,"penalty":0}
   ]

Membership.setUsersDefaultMembership(userIds, membership)
---------------------------------------------------------

Set the default membership for the given array of user id. Warning, the userId is not the same as the agentId.
'userIds' should be an array of user id like :

::

   [1, 2, 3]

'membership' should be an array of Queue membership like:

::

   [
     {"queueId":8,"penalty":1},
     {"queueId":17,"penalty":0},
     {"queueId":18,"penalty":0},
     {"queueId":23,"penalty":0}
   ]

Membership.applyUsersDefaultMembership(userIds)
-----------------------------------------------

Apply the existing default configuration to a set of users. Warning, the userId is not the same as the agentId.
'usersIds' should be an array of userId like:

::
  
   [1, 2, 7, 9]

Membership Events
=================

User default membership
-----------------------

Received when calling `Membership.getUserDefaultMembership(userId)`_.

* USERQUEUEDEFAULTMEMBERSHIP: "UserQueueDefaultMembership"

::

   {
     "userId":186,
     "membership": [
       {"queueId":8,"penalty":1},
       {"queueId":17,"penalty":0},
       {"queueId":18,"penalty":0},
       {"queueId":23,"penalty":0}
     ]
   }
