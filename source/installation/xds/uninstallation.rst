Media Server Uninstallation
===========================

This procedure is to remove a media server permanently.

On XIVO
-------

#. Disassociate all Users/Groups/SIP Trunk from the Media server you want to remove (remove these objects or associate them to another MDS),
#. Then remove the media server from XDS by removing it from webi (if needed refer to related sections in :ref:`xds_xivo-configuration`):

   * remove the media server in :menuselection:`Configuration -> Management -> Media Servers`
   * remove the corresponding template line in :menuselection:`Configuration -> Provisioning -> Template Line`
#. Clean connection permissions from all places and apply new configuration (see related sections in :ref:`xds_xivo-configuration`):

   * in AMI configuration
   * in Postgres conf (pg_hba.conf)
   * and in SMTP conf if applicable
#. And remove replication slot on XiVO Main server. Connect to postgres database find the correct replication slot and drop it::

    select * from pg_replication_slots;
    select pg_drop_replication_slot('slot_name');


On Media Server
---------------

Only reinstallation is supported.


Media Server Reinstallation
===========================

On XIVO
-------

All settings can remain if they are still valid - if the new media server has the same name and IP
as a previous (for example crashed) media server.

On Media Server
---------------

If the mds install script failed with error, you can re-run it.

But to fully reinstall media server, you need to reinstall xivo-mds-installer package
and recreate database on the media server.

* Uninstall MDS:

  .. code-block:: bash

    xivo-service stop all
    apt purge xivo-mds-installer
    rm -rf /var/lib/postgresql/11

* Reinstall MDS: the :file:`custom.env` will be recreated with new values:

  .. code-block:: bash

    apt install xivo-mds-installer
    xivo-service start all
    xivo-dcomp up -d

At the end you need also to reinstall the *XiVO Main* ssh public key for :ref:`xds_file_sync`:

.. code-block:: bash

  XIVO_HOST=$(grep XIVO_HOST /etc/docker/mds/custom.env | awk -F "=" '/1/ {print $2}')
  mkdir -p /root/.ssh
  wget https://$XIVO_HOST/ssh-key --no-check-certificate -O /root/.ssh/rsync_xds.pub
  cat /root/.ssh/rsync_xds.pub >> /root/.ssh/authorized_keys
  apt install -y rsync
