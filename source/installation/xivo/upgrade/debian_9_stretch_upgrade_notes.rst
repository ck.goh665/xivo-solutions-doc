.. _debian_9_upgrade_notes:

********************************
Debian 9 (stretch) Upgrade Notes
********************************

Debian was upgraded to Debian 9 (stretch) in XiVO 2018.14 release.

.. warning:: Upgrade from versions XiVO 15.19 or earlier are not supported.
     You MUST first upgrade to at least XiVO 15.20 or, more recommended, to XiVO Five
     before upgrading to XiVO Borealis.

.. warning:: Docker storage driver changed from ``aufs`` to ``overlay2``. To not suffer data loss you must follow a
   manual procedure which is not documented yet. Therefore:

     * XiVO PBX / UC / CC is **not installable or upgradable on XFS partition created without** ``ftype=1`` **option**.
       If the partition is XFS, you **MUST** check if the option is enabled with the ``xfs_info`` command.
     * Upgrade for **XiVO CC** to Debian 9 **is currently not supported**.

.. _debian_9_upgrade_notes_before:

Before the upgrade
==================

* It is not possible to upgrade from XiVO 15.19 or earlier. You first need to upgrade to XiVO Five.
* **Make sure you have sufficient space for the upgrade**. You should have more than 2GiB available in the filesystem that holds the ``/var`` and ``/`` directories.
* Note that the upgrade will take longer than usual because of all the system upgrade.
* You **MUST** deactivate all non-xivo apt sources list:

  * in directory `/etc/apt/sources.list.d/` you should only have the files :file:`xivo-dist.list` and (from Aldebaran) :file:`docker.list`.
  * you **MUST** suffix all other files with `.save` to deactivate them.
* You **MUST** check the Debian sources list are correct: the file :file:`/etc/apt/sources.list` must contain the following and only the following::

    deb http://ftp.fr.debian.org/debian/ jessie main
    deb-src http://ftp.fr.debian.org/debian/ jessie main

    deb http://security.debian.org/ jessie/updates main
    deb-src http://security.debian.org/ jessie/updates main

    # jessie-updates, previously known as 'volatile'
    deb http://ftp.fr.debian.org/debian/ jessie-updates main
    deb-src http://ftp.fr.debian.org/debian/ jessie-updates main

* You may want to clean your system before upgrading:

  * Remove package that were automatically installed and are not needed anymore::

     apt-get autoremove --purge
  * Purge removed packages. You can see the list of packages in this state by running ``dpkg -l |
    awk '/^rc/ { print $2 }'`` and purge all of them with::

      apt-get purge $(dpkg -l | awk '/^rc/ { print $2 }')
  * Remove :file:`.dpkg-old`, :file:`.dpkg-dist` and :file:`.dpkg-new` files from previous upgrade. You can see a list
    of these files by running::

      find /etc -name '*.dpkg-old' -o -name '*.dpkg-dist' -o -name '*.dpkg-new'


.. _debian_9_upgrade_notes_after:

After the upgrade
=================

* After having check your network configuration and the grub configuration, you **MUST** *reboot your system*. It is necessary for the upgrade to the Linux kernel to be effective.
* Check that customization to your configuration files is still effective.

  During the upgrade, new version of configuration files are going to be installed, and these might
  override your local customization. For example, the vim package provides a new :file:`/etc/vim/vimrc`
  file. If you have customized this file, after the upgrade you'll have both a :file:`/etc/vim/vimrc` and
  :file:`/etc/vim/vimrc.dpkg-old` file, the former containing the new version of the file shipped by the vim
  package while the later is your customized version. You should merge back your customization into
  the new file, then delete the :file:`.dpkg-old` file.

  You can see a list of affected files by running ``find /etc -name '*.dpkg-old'``. If some files
  shows up that you didn't modify by yourself, you can ignore them.

* Purge removed packages. You can see the list of packages in this state by running ``dpkg -l |
  awk '/^rc/ { print $2 }'`` and purge all of them with::

    apt-get purge $(dpkg -l | awk '/^rc/ { print $2 }')


Changes
=======

Here's a non-exhaustive list of changes that comes with XiVO on Debian 9:

* Network utility: the tools from the ``net-tools`` package are no longer part of new installations by default. They are replaced by the ``iproute2`` toolset.
  For a complete summary of the net-tools commands with their iproute2 equivalent 
  see the `Official Debian 9 Release notes related chapter <https://www.debian.org/releases/stable/amd64/release-notes/ch-information.en.html#iproute2>`_.
  Here are 4 examples:

   * Instead of arp, use ``ip n`` (short for ``ip neighbor``)
   * Instead of ifconfig, use ``ip a`` (short for ``ip addr``)
   * Instead of netstat, use ``ss``
   * Instead of route, use ``ip r`` (short for ``ip route``)
* Docker recommended storage driver is ``overlay2`` and the usage of ``aufs`` storage driver was deprecated in Debian 9 (stretch).
  Thus we recommend to use ``overlay2`` as storage driver and our upgrade procedure will try to stick to this choice if possible.
  (Note that, via a manual procedure and installation of extra packages, it is still possible to use ``aufs`` storage driver but we do not recommend it).


External Links
==============

`Official Debian 9 Release Notes <https://www.debian.org/releases/stretch/releasenotes>`_

