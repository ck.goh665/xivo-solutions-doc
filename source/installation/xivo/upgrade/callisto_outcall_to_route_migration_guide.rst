.. _callisto_route_upgrade_guide:

******************************
Outcall to Route Upgrade Guide
******************************

This page gives some indication to migrate the Outgoing Calls configuration to the new :ref:`Routes introduced in LTS Callisto <outgoing_calls>`.

Deal with the warning in migration script
=========================================

Could not find an internal context matching the outgoing context
----------------------------------------------------------------

Outcall WAS NOT MIGRATED TO ROUTE because it is deactivated ib db
-----------------------------------------------------------------


Deal with your configuration
============================
